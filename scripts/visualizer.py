#!/usr/bin/env python
import rospy
from std_msgs.msg import String, Int8
from geometry_msgs.msg import Point
from sensor_msgs.msg import PointCloud
import message_filters

import numpy as np
from matplotlib import pyplot as plt

class Visualiser:
 def __init__(self):
    x = np.linspace(0, 2* np.pi, 100)
    y = np.random.random_integers(1, 100, 100)
    plt.ion()
    self.fig = plt.figure()
    ax = self.fig.add_subplot(111)
    self.line1, = ax.plot(x, y, 'b-')

 def scanner_callback(self, scan):
    distance_list = scan.ranges
    print(distance_list)
    print("scan angle min", scan.angle_min)
    print("scan angle incr", scan.angle_increment)
    print("scan angle max", scan.angle_max)
    self.line1.set_ydata(distance_list)
    self.fig.canvas.draw()


rospy.init_node('pool_visual_node')
vis = Visualiser()
sub = rospy.Subscriber('\scan', LaserScan, vis.scanner_callback)
rospy.spin()