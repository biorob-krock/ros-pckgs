#!/bin/bash

# URDF_playback.sh
# Matt Estrada March 2020
# Play back sample bagfile for testing krock2 visualization 

BAG_NAME="pool-krock-preliminary-2020-03-12_19-28-35.bag"
roslaunch krock_pckgs review_pool_experiment.launch bagfile_name:=$(pwd)/../data/bags/$BAG_NAME
printf "Please select bagfile:\n"
select BAG_NAME in "../data/bags"/*".bag"; do test -n "${BAG_NAME%"data"/*}" && break; echo ">>> Invalid Selection"; done
roslaunch krock_pckgs review_pool_experiment.launch bagfile_name:=$(pwd)/$BAG_NAME
