ROS Packages
------------

Keeping track of all the packages we\'ve implemented (or at least
tested) on the Krock 2 hardware

## Dynamixel Control

Useful links

-   [ROS page](http://wiki.ros.org/dynamixel)
-   [tutorial](http://emanual.robotis.com/docs/en/software/dynamixel/dynamixel_workbench/#ros-tutorials)

#### Adrien Notes

-   Install package :
    `sudo apt-get install ros-melodic-dynamixel-workbench`
-   Search for your dynamixels (if not found check your connectivity and
    power supply). Use `ls /dev/ttyUSB*` to search your port, give it
    permissions with `chmod a+rw /dev/ttyUSB0`:

```{=html}
<!-- -->
```
    rosrun dynamixel_workbench_controllers find_dynamixel /dev/ttyUSB0 

-   roscd dynamixel\_workbench\_controllers
-   In the config folder you can write your own yaml file. You can refer
    to dynamixel control table and to other config files in the folder
    as examples. WARNING: Torque\_Enable isn't supposed to be set by
    users, but it's enabled by itself during initialization. (Note : you
    can see the list of parameters of the control table in the
    corresponding rosmsg with rosmsg info dynamixel\_workbench\_msgs/RX
    for dynamixel-RX)
-   Modify the launch file according to your config (modify baud\_rate
    and basic.yaml name)
-   Start the nodes: roslaunch dynamixel\_workbench\_controllers
    dynamixel\_controllers.launch
-   You can now use the service /dynamixel\_command to command the
    dynamixel :

```{=html}
<!-- -->
```
    rosservice call /dynamixel_workbench/dynamixel_command "command: ''
    id: 31
    addr_name: 'Goal_Position'
    value: 512"

Use the joint\_operator to move the servo with the PS4 controller :

-   roscd dynamixel\_workbench\_operators
-   Create your config file and update the launch file
-   (if not already installed : sudo apt-get install ros-melodic-joy)

### Communication between PC and Odroid

[Tutorial](http://wiki.ros.org/ROS/Tutorials/MultipleMachines) for
running ROS on multiple machines. Problems are most likely to stem from
[network](http://wiki.ros.org/ROS/NetworkSetup) configurations.

-   add \'odroid\_ip odroid\' in the /etc/hosts file in the PC (fill the
    odroid\_ip with the updated one from
    <https://biorob2.epfl.ch/pages/internal/dynip.php>)
-   add \'pc\_ip nccr-local-linux\' in the /etc/hosts file in the Odroid
    (here the name and the ip must be set according to PC used)
-   when starting the roscore on the master we can see the
    ROS\_MASTER\_URI that we have to set for both nodes by \'export
    ROS\_MASTER\_URI=<http://nccr-local-linux:11311>\' (here again the
    hostname depends of the host)
-   Possibility to set the ROS\_MASTER\_URI for all new shells :
    1.  echo \"export
        ROS\_MASTER\_URI=http://nccr-local-linux:11311\" \>\> \~/.bashrc
    2.  source \~/.bashrc
-   If topic communication does not work between machines check that
    ROS\_IP is well defined (echo \$ROS\_IP) otherwise define it (export
    ROS\_IP=\<machine\_ip\_addr\>)

```{=html}
<!-- -->
```
    * 

## Seek thermal camera

#### Driver and node

-   [Website](https://www.thermal.com/compact-series.html)
-   [ROS node for the
    drivers](https://github.com/coaco-robot/seek_compact_pro_node)
    -   [Drivers
        alone](https://github.com/coaco-robot/libseek-thermal/tree/e4f8eaa0a4b2ef18e9b244dc3089df9691d9db19)

#### Modifications

-   Follow the drivers installation, and test with `seek_test` example
-   Out-of-repository is set up to run the Seek Compact Pro, but we have
    the Seek Compact
    -   `[ERROR] [1567078175.273579393]: Unable to open SEEK Compact camera`
    -   Modify the CMakeList.txt line 141 to read
        `add_executable(libseek_thermal_driver src/compact.cpp)`
    -   Created file `compact.cpp` with line 21 now initializing
        `LibSeek::SeekThermal seek("");` instead of the SeekThermalPro
-   This was not too painful of a fix since the
    [drivers](https://github.com/coaco-robot/libseek-thermal/tree/e4f8eaa0a4b2ef18e9b244dc3089df9691d9db19#getting-usb-access)
    differentiate between the compact (0010) and the compact pro (0011).
    This difference shows up when listing with `lsusb`

#### Running

    roscore
    rosrun libseek_thermal_driver libseek_thermal_driver 
    rosrun image_view image_view image:=/seek/image

Here I\'m running with [image\_view](http://wiki.ros.org/image_view)
which may be part of a larger
[image\_pipeline](http://wiki.ros.org/image_pipeline)? Alternatives seem
to be [rqt\_image\_view](http://wiki.ros.org/rqt_image_view) and
[image\_transport](http://wiki.ros.org/image_transport). I tried a
[tutorial](http://wiki.ros.org/image_transport/Tutorials/SubscribingToImages)
to script up a simple image\_transport subscriber, but got some error.
[Some
googling](https://github.com/ros-perception/image_pipeline/issues/201)
suggested it was a problem with GTK, but it worked fine with
image\_viewer.

    (view:26311): GLib-GObject-CRITICAL **: 15:56:05.266: g_object_unref: assertion 'G_IS_OBJECT (object)' failed
    Attempt to unlock mutex that was not locked
    Aborted (core dumped)

## MvBlueFOX camera

Keeping track of this in
[odroid-ros](/projects/krock-2/troubleshooting/odroid-ros)

-   Website :
    <https://www.matrix-vision.com/USB2.0-single-board-camera-with-housing-mvbluefox-igc.html>
-   ROS wiki : <https://wiki.ros.org/mv_bluefox_driver>
-   ROS API github : <https://github.com/KumarRobotics/bluefox2>

#### Setup

-   Install drivers by following these instructions
    <https://www.matrix-vision.com/manuals/mvBlueFOX/mvBF_page_quickstart.html#mvBF_section_quickstart_linux>
    -   The driver archive and installer can be found
        [here](https://www.matrix-vision.com/USB2.0-single-board-camera-with-housing-mvbluefox-igc.html)
        under downloads
    -   **Note**: for ODROID use the ARM 32bit version. When running the
        ./install.sh script make sure to use `sudo`. If you forget this
        step you have to redownload the tar file and start over
-   Clone dependent package in the workspace:
    <https://github.com/KumarRobotics/camera_base>
-   Clone ROS API for the camera in the workspace:
    <https://github.com/KumarRobotics/bluefox2>
-   catkin\_make

#### Run

    roslaunch bluefox2 single_node.launch device:=25000821

      * The first time you run it if you don't have the serial number just write 0 instead and look it will be displayed in the terminal
      * Afterwards if you always use this camera you can modify the launch file to directly specify the device arg
    * Now the topic /mv_serial_num/image_raw contains your data (you can display it with the image view plugin in rqt)

#### View

Eaiest way to view is with image\_view but it seems to only be giving me
1/4 of the shot

    rosrun image_view image_view image:=/mv_25000821/image_raw

#### Communicating across machines

Also add ROS\_IP=\<machine\_ip\_addr\> if you want to communicate across
different machines.

### Data Collection

3 possibilities :

-   Matt uses .txt files to log data. We can read those files and feed
    it to ROS
-   Tomislav code uses shared memory for several data storing -\>
    collect it for ROS
-   Why not just define a background ROS publisher that send all the
    sensors data at a given rate (in fact exactly the same way as the
    log files are written but publish to ROS topic instead)

During exploration on shared memory topic I found this package
<http://wiki.ros.org/shm_transport> that can be used to reduce latency
while communicating through ROS topics (especially for large size data).

##### Content of the GUI :

<https://gitlab.com/biorob-krock/Krock2_RobotController/blob/master/source/controller_logging.cpp>

    Tab 1
    Commanded (dotted line possible?) vs. Measured Angles (solid)
    Commanded angles variable: angles[i]
    Position angles variable: fbck_angles[i]

    Give me six plots with commanded/measured in the same color but different line styles:

    Motors 0-3 Left Front
    Motors 4-7 Right Front
    Motors 8-11 Left Hind
    Motors 12-15 Right Hind
    Motors 16, 17 Trunk
    Motors 18-20 Tail

    Tab 2 
    Motor Currents 
    Current variable: fbck_torques[i] (Torques is a horrible variable name for this, it's current)
    Currents corresponding to the split defined above (e.g. 0-3 on same graph, 4-7, ...) 

    Tab 3
    Forces 
    Forces variable: nnEstForce
    Four force plots for forces with XYZ measurements ("measurements" are actually estimates here). 

    Tab 4
    IMU 
    IMU variable: forRPY (a.k.a. roll, pitch, yaw) 
    One plot with RPY IMU angle measurement
    Plot with XYZ Accelerations
    Plot with XYZ Angular Rates 

Best GUI plugin ever made?
<https://github.com/ANYbotics/rqt_multiplot_plugin>

## Logitech c920 and usb_cam

### Install usb_cam 

    sudo apt install ros-melodic-usb-cam

    sudo apt install ros-melodic-image-view

### Logitech Camera

[Logitech c920](https://www.logitech.com/en-ch/product/hd-pro-webcam-c920) webcam and viewing through 

    roslaunch usb_cam usb_cam-test.launch 

[image_view](http://wiki.ros.org/image_view#image_view.2BAC8-diamondback.video_recorder) has a useful video stream capture named video_recorder

    rosrun image_view video_recorder image:=/usb_cam/image_raw

To just **view** is simpler 

    rosrun image_view image_view image:=/usb_cam/image_raw


[Refocusing](https://www.youtube.com/watch?v=94lDYZgihT4) can be done manually by unscrewing housing and twisting lense. 

### Other

[MoveIt](http://wiki.ros.org/moveit) looks interesting for motion
planning.
