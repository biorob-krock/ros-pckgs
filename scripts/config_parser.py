import libconf
import io
import os
import json

def find(name, path):
    for root, dirs, files in os.walk(path):
        if name in files:
            return os.path.join(root, name)

path = find('GLOBAL_WEBOTS.cfg', '/home/francisco')

with io.open(path, encoding='utf-8') as f:
    cfg = libconf.load(f)
    string = json.dumps(cfg)
    print(string)
