#!/usr/bin/env python
import rospy
from std_msgs.msg import String, Int8
from sensor_msgs.msg import JointState
import message_filters

import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation
from matplotlib.patches import Rectangle

VCC = 16 # V 

def callback(jointMeasuredData):
    nJoints = len(jointMeasuredData.position)
    print("Number of joints: ", nJoints)
    power = [None]*nJoints
    totalPower = 0 
    for i in range(nJoints): 
        power[i] = VCC * abs(jointMeasuredData.effort[i])
        totalPower += power[i]
    
    print("Total Power: ", totalPower) 

def listener():

    rospy.init_node('energy_calc', anonymous=True)
    rospy.Subscriber("/krock2/jointMeasuredData", JointState, callback)

    plt.show()
    rospy.spin()

if __name__ == '__main__':
    listener()