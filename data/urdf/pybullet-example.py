import time
import pybullet


def main():
    """Main"""
    pybullet.connect(pybullet.GUI)
    plane = pybullet.createCollisionShape(pybullet.GEOM_PLANE)
    pybullet.createMultiBody(0, plane)
#    krock = pybullet.loadURDF('krock_final.urdf')
    krock = pybullet.loadURDF('urdf-krock-pybullet.urdf')
    pybullet.resetBasePositionAndOrientation(krock, [0, 0, 1], [0, 0, 0, 1])
    pybullet.setGravity(0, 0, -9.81)
    tic = time.time()
    while True:
        toc = time.time()
        pybullet.setTimeStep(toc-tic)
        tic = time.time()
        pybullet.stepSimulation()


if __name__ == '__main__':
    main()
