"""Gather information from robot bagfile and plot for analysis 

Usng Python 2 for compatibility with ROS Melodic rospy and rosbag modules 
"""


import re

import ipdb
import numpy as np
import rosbag
import rospy
import sensor_msgs.msg
import yaml
from colorama import Back, Fore, Style  # color printouts
from matplotlib import pyplot as plt
from numpy import linalg as LA
from rosbag.bag import Bag
from scipy import integrate, signal
from sensor_msgs.msg import Imu, JointState
from tqdm import tqdm  # progress bar

# from scipy.fft import fft



class JointStash:
    """ Temporary container while vectorizing joint data
    """
    def __init__(self):
        self.t = []
        self.name = []
        self.effort = []  # For more info
        self.veolcity = []
        self.name = []


class KrockBag:
    """ Hold vectorized rosbag messages from different topics
    """
    def __init__(self, filename, **kwargs):
        """ Import bagfile

        Parameters:

        filename (str): path to bagfile
            partial_bag (float) [optional kwarg]: partial duration (starting from beginning of bagfile) to plot if desired

        Returns: 
            void: data is kept in the class itself with methods used for plotting

        """
        self.jointMeasured = []  # stash ros messages
        self.jointCommanded = []  # stash ros messages
        self.imu = []
        self.joint_effort = []
        self.joint_position = []
        self.t = []
        self.joint_name = []

        file_clip = re.match('^.*/(.*\.bag)$',
                             filename)  # strip off directory from filename

        print("\nloading bagile: " + Fore.YELLOW + file_clip.group(1) +
              Style.RESET_ALL
              )  # second ground in re displays first capture group
        bag = rosbag.Bag(filename)

        t0 = bag.get_start_time()
        tf = bag.get_end_time()

        dt = tf - t0
        nMsg = bag.get_message_count()

        startTime = rospy.Time(t0 + 0.1)
        if "partial_bag" in kwargs:

            endTime = rospy.Time(t0 + kwargs.get("partial_bag"))
        else:
            endTime = rospy.Time(tf)

        # For more info
        # info_dict = yaml.load(bag._get_yaml_info())

        print("\nDuration: \t\t" + Fore.YELLOW + str(dt) + Style.RESET_ALL)
        print("Start time: \t\t" + Fore.CYAN + str(t0) + Style.RESET_ALL
              )  # second ground in re displays first capture group
        print("End time: \t\t" + Fore.CYAN + str(tf) + Style.RESET_ALL
              )  # second ground in re displays first capture group
        print("Number of messages: \t" + Fore.CYAN + str(nMsg) +
              Style.RESET_ALL
              )  # second ground in re displays first capture group

        if "partial_bag" in kwargs:
            print("Parsing partial bag: \t" + Fore.YELLOW +
                  str(kwargs.get("partial_bag")) + Style.RESET_ALL
                  )  # second ground in re displays first capture group
        print("")

        topic = ['']

        def t2fl(obj):
            return float(obj.secs) + float(obj.nsecs) * 10**-9 - float(t0)

        with tqdm(total=nMsg) as pbar:
            for topic, msg, t in bag.read_messages(topics=[
                    '/krock2/jointMeasuredData', '/krock2/jointCommand',
                    '/krock2/imuRPY'
            ],
                                                   start_time=startTime,
                                                   end_time=endTime):
                t_float = t2fl(t)
                if topic == '/krock2/jointMeasuredData':
                    jointNow = JointStash()
                    jointNow.t = t_float
                    jointNow.effort = msg.effort
                    jointNow.velocity = msg.velocity
                    jointNow.position = msg.position
                    jointNow.name = msg.name
                    self.jointMeasured.append(jointNow)

                elif topic == '/krock2/jointCommand':
                    self.jointCommanded.append(msg)

                elif topic == '/krock2/imuRPY':
                    self.imu.append(msg)
                pbar.update(1)  # update progress bar
                pbar.set_description("ROS time [s]: %d" % t_float)

            bag.close()

        print("\n" + Fore.CYAN + 'Parsed Bagfile' + Style.RESET_ALL)
        nJointMsg = len(self.jointMeasured[:])
        print("Number of joint msgs: \t" + Fore.CYAN + str(nMsg) +
              Style.RESET_ALL +
              "\n")  # second group in re displays first capture group

        # Transfer data from ROS msgs to arrays
        nJoints = 21
        jointPos = np.empty([nJointMsg, nJoints])
        jointEff = np.empty([nJointMsg, nJoints])
        jointVel = np.empty([nJointMsg, nJoints])
        self.t = np.empty([nJointMsg, 1])

        for ii in range(nJointMsg):
            thisMsg = self.jointMeasured[ii]
            self.t[ii] = thisMsg.t
            for jj in range(nJoints):
                jointPos[ii][jj] = thisMsg.position[jj]
                jointEff[ii][jj] = thisMsg.effort[jj]

        first_msg = self.jointMeasured[0]
        self.joint_name = [None] * nJoints
        for jj in range(nJoints):
            thisMsg = self.jointMeasured[0]
            self.joint_name[jj] = first_msg.name[jj]

        self.joint_effort = jointEff
        self.joint_position = jointPos

    def gen_plot(self, *args):
        """ Streamlining duplicate plotting code"""

        n_col = 5
        n_row = 1

        appendages = ['FL', 'FR', 'HL', 'HR', 'Spine']

        for plotcase in args:
            plt.figure()

            print("Plotting: \t" + Fore.CYAN + plotcase + Style.RESET_ALL)

            for leg in appendages:

                if leg == 'FL':
                    ind = range(3)
                    subInd = 1

                elif leg == 'FR':
                    ind = range(4, 8)
                    subInd = 2

                elif leg == 'HL':
                    ind = range(8, 12)
                    subInd = 3

                elif leg == 'HR':
                    ind = range(12, 16)
                    subInd = 4

                elif leg == 'Spine':
                    ind = range(16, 21)
                    subInd = 5

                if plotcase is 'limb_effort':
                    # Summation of limb effort together
                    this_limb_slice = self.joint_effort[:, ind]
                    this_limb_effort = np.sum(this_limb_slice, axis=1)
                    plt.plot(self.t, this_limb_effort, label=leg)

                    plt.title('Limb effort')
                    plt.xlabel('[sec]')
                    plt.ylabel(leg + ' [A]')
                    plt.legend()

                elif plotcase is 'motor_current':
                    # Visual individual motor currents
                    plt.subplot(n_col, n_row, subInd)
                    for i in ind:
                        plt.plot(self.t,
                                 self.joint_effort[:, i],
                                 label=self.jointMeasured[0].name[i])

                    plt.title('Appendage effort')
                    plt.xlabel('[sec]')
                    plt.ylabel('[A]')
                    plt.legend()

                elif plotcase is 'energy':
                    plt.subplot(2, 1, 1)
                    joint_effort_cumsum = integrate.cumtrapz(np.absolute(
                        self.joint_effort),
                                                             self.t,
                                                             axis=0,
                                                             initial=0)
                    plt.plot(self.t, joint_effort_cumsum)

                    plt.title("Integrated joint effort")
                    plt.xlabel("[sec]")  # plt.grid()

                    plt.subplot(2, 1, 2)
                    plt.plot(self.t,
                             np.sum(joint_effort_cumsum, axis=1),
                             label='total')
                    plt.title('total energy')

                elif plotcase is 'filter_test':
                    # Figuring out appropriate current measurements
                    # # b, a = signal.butter(4, 0.125)

                    TEST_AXIS = 2
                    b, a = signal.butter(30, 0.15, 'low', analog=True)

                    w, h = signal.freqs(b, a)
                    plt.semilogx(w, 20 * np.log10(abs(h)))
                    plt.title('Butterworth filter frequency response')
                    plt.xlabel('Frequency [radians / second]')
                    plt.ylabel('Amplitude [dB]')
                    plt.margins(0, 0.1)
                    plt.grid(which='both', axis='both')
                    plt.axvline(100, color='green')  # cutoff frequency

                    plt.figure()
                    joint_effort_filt = signal.filtfilt(
                        b, a, self.joint_effort[:, TEST_AXIS].T)
                    plt.plot(self.t,
                             self.joint_effort[:, TEST_AXIS],
                             label='raw')
                    plt.plot(self.t, joint_effort_filt,
                             label='filtered')  # plt.grid()

                    plt.ylabel('[A]')
                    plt.legend()

                elif plotcase is 'filter_test_sos':
                    TEST_AXIS = 2
                    sos = signal.ellip(13, 0.009, 80, 0.05, output='sos')
                    y_sos = signal.sosfiltfilt(
                        sos, self.joint_effort[:, TEST_AXIS].T)
                    plt.plot(self.t,
                             self.joint_effort[:, TEST_AXIS],
                             label='raw')
                    plt.plot(self.t, y_sos, label='filtered')
                    plt.title('Filtering with sos filter')
                    plt.xlabel('[sec]')
                    plt.ylabel('[A]')
                    plt.legend()

                elif plotcase is 'fft':
                    TEST_AXIS = 2
                    y = self.joint_effort[:, TEST_AXIS].T
                    # Number of sample points
                    # sample spacing

                    dt = 1.0 / 100.0
                    n = len(y)
                    yf = np.fft.fft(y)
                    yfreq = np.fft.fftfreq(n, dt)

                    plt.ylabel("Amplitude")
                    plt.xlabel("Frequency [Hz]")
                    plt.bar(yfreq[:n // 2],
                            np.abs(yf)[:n // 2] * 1 / n,
                            width=0.2)


# fig = plt.figure()
# jointEff_hist = np.histogram(jointEff, bins=100, range = [0,20])
# plt.hist(jointEff_hist, bins='auto')

# ipdb.set_trace()
