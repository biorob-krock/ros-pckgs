# Krock Package 

Evolving repository for useful ROS packages for Krock-2 Robot.
These are to be run on an accompanying PC, not the ODROID.

This repo should be cloned under the `catkin_ws/src/` folder so that the `catkin_make` command can find and build the packages.
The nodes show up under the `krock_pckgs` package.

Large bag files in `data/` are track with [git LFS](https://git-lfs.github.com/), so install this as well. 

## Running with Krock-2

Make sure the toggle `USE_ROS` is set to `true` in the `GLOBAL.cfg` config file. Next run the robot with a prefix setting the location of the ROS master. 

    ROS_MASTER_URI=http://biorobpc25:11311 ROS_IP=<currentIP> ./robot_main 

The `ROS_MASTER_URI` environment variable is printed on the screen when you start `roscore` (`biorobpc25` above is for Matt's PC). 

Printing out `rostopic list` on the rosmaster computer should list krock2 topics: 

    /krock2/controlCommand
    /krock2/forceVisu
    /krock2/gait
    /krock2/imuRPY
    /krock2/jointCommand
    /krock2/jointMeasuredData
    /krock2/legForce/bl
    /krock2/legForce/br
    /krock2/legForce/fl
    /krock2/legForce/fr
    /krock2/pose
    /krock2/positionTarget
    /krock2/requestedState

Note that `ROS_MASTER_URI` is also defined in `~/.bashrc` at the moment. 

## Structure

- `data/` - useful, cherry-picked bagfiles and script for saving to csv
- `launch/` - launch files
- `parse/` - running/parsing scripts for data (mostly bash)
- `scripts/` - nodes in python 
- `src/` - C++ nodes
- `bagfiles/` - local storage for temporary placement bagfiles, inlcuded into `.gitignore` so they won't be committed. Move these to `/data` if they are important

Temporary folders include: 

- `/adrien_packages` - several exploratory packages done by semester student Adrien Chassignet
- `/config_files` - configs for Adrien's packages

## RQT Dashboard
Here is a video tutorial:
[Tutorial on installation and use of RQT](https://drive.google.com/file/d/1W4v_fT2jiTCSBDUGAWM0ELX1d9MMt0zr/view?usp=sharing)
### Installation
Run following commands to install external plugins:

```bash
sudo apt-get install ros-melodic-jsk-rqt-plugins
sudo apt-get install ros-melodic-jsk-recognition-msgs
```
Run: 
`catkin_make`
### Initialization 
Roscore always needs to be running for RQT to work, so in a new terminal:
```bash
roscore
```
run RQT with the force discover once so that the new plugins appear in the menu:
```bash
rqt --force-discover
``` 
Click on `Perspectives` -> `import...` and select `Krock-Visualization-Dashboard.perspective` found in `~/catkin_ws/src/ros-pckgs/config_files/`

### Dashboard usage 
For the plugin to work the `dashboard.launch` needs to be launched through the ROS launch GUI as explained below. Using the other plugins is optional and depends on the goal. 

<img src="./doc/dashboard/dash1.PNG"  width="549" height="386">
<img src="./doc/dashboard/dash2.PNG"  width="549" height="386">

* for rqt_bag (10) you can open the bag file that we want to visualize then `right-click on the timeline->publish->publish all`.
* for the ROS launch GUI (3) select `krock_pckgs` then `dashboard.launch` ~then click `LOAD PARAMS` and `START ALL`
* for rqt_rviz (9) click on `file->open config` and open `krock2_visualization.rviz` found in `~/catkin_ws/src/ros-pckgs/config_files/`
* for rqt_plot (1) you may write down the name of a topic to plot, for example, `/joint_states/position[1]`
* for Image View (8) click on `refresh` and then select `/usb_cam/image_raw`
* for jsk_service_button (12) click on `config` and select `package://krock_pckgs/config_files/service_button_layout.yaml`
* for jsk_string (2) click on the `config` and select: `/config_params/data` topic

Now you have the whole dashboard set up. Some of these settings are remembered, but others such as Rviz always need to be set up again.

### Tips:
Close the `Displays` `Time` and `Views` sections within rqt_rviz (9) for better visibility.

### Known issues:
Resizing Plot window crashes RQT, this happens with tiling managers for example.
Selecting the wrong path on the configuration options of plugins will also cause a crash.
The built in terminal freezes the dashboard with certain commands.

