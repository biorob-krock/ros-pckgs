#!/bin/bash

# pool_experiment.sh
# Matt Estrada	March 2020 	Lausanne, CH 

#LAUNCH_NAME="water_experiments.launch"

LAUNCH_NAME="stripped-experiments.launch"


#CYAN_IP=$(source ../include/krock_ssh.sh cyanip | tail -1) # Tail only grabs last line of echo 
#CYAN_IP='odroid-cyan'
ARCH=$(uname -m) # Are we on ODROID or PC?
echo $CYAN_IP

# Naming bagfile 
TRIAL_NAME="motion-capture-prelim"
DATE_TIME=$(date +"%Y-%m-%d_%H-%M-%S")
OUTDIR="/../data/bags/"
BAG_NAME=$(pwd)$OUTDIR$TRIAL_NAME-$DATE_TIME

source /data/estrada/catkin_ws/devel/setup.bash

# Enable ssh 
export ROSLAUNCH_SSH_UNKNOWN=1

# Do the deed 
# roslaunch krock_pckgs $LAUNCH_NAME krock2_ip:=$CYAN_IP trial_name:=$TRIAL_NAME bagfile_name:=$BAG_NAME --screen
roslaunch krock_pckgs $LAUNCH_NAME trial_name:=$TRIAL_NAME bagfile_name:=$BAG_NAME --screen
