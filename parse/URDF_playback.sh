#!/bin/bash

# URDF_playback.sh
# Matt Estrada March 2020
# Play back sample bagfile for testing krock2 visualization 

#BAG_NAME="urdf-playback-2020-03-11_15-49-39.bag"
#roslaunch krock_pckgs URDF_playback.launch bagfile_name:=$(pwd)/../data/bags/$BAG_NAME


#BAG_NAME="ROS-streamline-2020-05-03_19-19-11.bag"

BAG_NAME="swim-ramp-first-2020-05-20_11-38-47.bag"
roslaunch krock_pckgs URDF_playback.launch bagfile_name:=$(pwd)/../data/bags/$BAG_NAME
