#!/usr/bin/env python
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Vector3
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import TransformStamped
from geometry_msgs.msg import Point
from geometry_msgs.msg import Twist
from std_msgs.msg import String
from std_msgs.msg import Float64
from std_msgs.msg import Int32
from std_msgs.msg import Int16
from std_msgs.msg import Bool
from tf.transformations import *
#from beginner_tutorials.srv import *
from krock_pckgs.srv import *
import tf
import math
import numpy
import json
import io
import os


class BagToJointState:
    def __init__(self):
        joints_0_3 = ["solid_flpitch_hj_endpoint_to_FLpitch_T_FLpitch_HJ",
                      "solid_flyaw_hj_endpoint_to_solid_flpitch_hj_endpoint_FLyaw_HJ",
                      "FLroll_HJ_C_to_FLroll_T_FLroll_HJ",
                      "FLknee_HJ_C_to_FLroll_HJ_C_FLknee_HJ"]
        joints_4_7 = ["FRpitch_HJ_C_to_FRpitch_T_FRpitch_HJ",
                      "FRyaw_HJ_C_to_FRpitch_HJ_C_FRyaw_HJ", "FRroll_HJ_C_to_FRroll_T_FRroll_HJ",
                      "FRknee_HJ_C_to_FRroll_HJ_C_FRknee_HJ"]
        joints_8_11 = ["solid_hlpitch_hj_endpoint_to_HLpitch_T_HLpitch_HJ",
                       "HLyaw_HJ_C_to_solid_hlpitch_hj_endpoint_HLyaw_HJ", "HLroll_HJ_C_to_HLroll_T_HLroll_HJ",
                       "HLknee_HJ_C_to_HLroll_HJ_C_HLknee_HJ"]
        joints_12_15 = ["HRpitch_HJ_C_to_HRpitch_T_HRpitch_HJ",
                        "HRyaw_HJ_C_to_HRpitch_HJ_C_HRyaw_HJ", "HRroll_HJ_C_to_HRroll_T_HRroll_HJ",
                        "HRknee_HJ_C_to_HRroll_HJ_C_HRknee_HJ"]
        joints_20_33 = ["solid_spine1_endpoint_to_SPINE1_T_SPINE1_HJ", "solid_spine2_endpoint_to_SPINE2_T_SPINE2_HJ",
                        "solid_tail1_endpoint_to_TAIL1_T_TAIL1_T_C",
                        "solid_tail1_passive_endpoint_to_TAIL2_T_TAIL2_T_C",
                        "solid_tail3_endpoint_to_TAIL3_T_TAIL3_T_C"]
        
        self.odom_trans = TransformStamped()
        self.odom_trans.header.frame_id = "odom"
        self.odom_trans.child_frame_id = "base_link"

        self.requestedState = Int32()
        self.gait = Float64()
        self.positionTarget = Point()
        self.controlCommand = Point()

        self.robotState = Int16()
        self.configSim = Bool()
        self.configHighLev = Bool()
        self.configJoy = Bool()
        self.configMotors = Bool()

        self.joint_state = JointState()
        self.joint_errors = JointState()
        self.RPY = Vector3()
        self.pose = PoseStamped()
        self.joint_state.name = joints_0_3 + joints_4_7 + \
                                joints_8_11 + joints_12_15 + joints_20_33

        self.config_pub = rospy.Publisher(
            'config_params', String, queue_size=1000)
        self.joint_pub = rospy.Publisher(
            "joint_states", JointState, queue_size=1000)
        self.joint_error_pub = rospy.Publisher(
            "joint_errors", JointState, queue_size=1000)


        self.control_command_pub = rospy.Publisher("/krock2/controlCommand", Point, queue_size=1000)
        self.requested_state_pub = rospy.Publisher("/krock2/requestedState", Int32, queue_size=1000)
        self.gait_pub = rospy.Publisher("/krock2/gait", Float64, queue_size=1000)
        self.position_target_pub = rospy.Publisher("/krock2/positionTarget", Point, queue_size=1000)

        self.bag_sub = rospy.Subscriber("/krock2/jointMeasuredData", JointState,
                                        self.bag_info_callback, queue_size=1000)
        self.imu_sub = rospy.Subscriber("/krock2/imuRPY", Vector3,
                                        self.rpy_callback, queue_size=1000)
        self.pose_sub = rospy.Subscriber("/krock2/pose", PoseStamped,
                                         self.pose_callback, queue_size=1000)
        self.command_sub = rospy.Subscriber("/krock2/jointCommand", JointState,
                                            self.command_info_callback, queue_size=1000)
        self.requested_state_sub = rospy.Subscriber("/krock2/requestedState", Int32,
                                                    self.requested_state_callback, queue_size=1000)
        self.gait_sub = rospy.Subscriber("/krock2/gait", Float64,
                                         self.gait_callback, queue_size=1000)
        self.position_target_sub = rospy.Subscriber("/krock2/positionTarget", Point,
                                                    self.position_target_callback, queue_size=1000)
        self.control_command_sub = rospy.Subscriber("/krock2/controlCommand", Point,
                                                    self.control_command_callback, queue_size=1000)
        self.cmd_vel_sub = rospy.Subscriber("/cmd_vel", Twist,
                                                    self.cmd_vel_callback, queue_size=1000)                                            

        self.m_subRobotState = rospy.Subscriber("/krock2/robotState", Int16, self.robot_state_sub_callback, 1000)
        self.m_subConfigSim = rospy.Subscriber("/krock2/config/simulation", Bool, self.config_sim_callback, 1000)
        self.m_subConfigHighLev = rospy.Subscriber("/krock2/config/highLevelController", Bool, self.config_high_lev_callback, 1000)
        self.m_subConfigJoy = rospy.Subscriber("/krock2/config/enable_joystick", Bool, self.config_joy_callback, 1000)
        self.m_subConfigMotors = rospy.Subscriber("/krock2/config/motorsDisable", Bool, self.config_motors_callback, 1000)

        self.service_server_0 = rospy.Service('click_button_zero', button, self.handle_button_state_0)
        self.service_server_1 = rospy.Service('click_button_one', button, self.handle_button_state_1)
        self.service_server_2 = rospy.Service('click_button_two', button, self.handle_button_state_2)
        self.service_server_3 = rospy.Service('click_button_three', button, self.handle_button_state_3)
        self.service_server_4 = rospy.Service('click_button_four', button, self.handle_button_state_4)
        self.service_server_5 = rospy.Service('click_button_five', button, self.handle_button_state_5)
        self.service_server_6 = rospy.Service('click_button_six', button, self.handle_button_state_6)
        self.service_server_7 = rospy.Service('click_button_seven', button, self.handle_button_state_7)           
        self.trans_broadcaster = tf.TransformBroadcaster()

        rate = rospy.Rate(10)  # 10hz

        while not rospy.is_shutdown():
            # config_str = "# Mode\nIS_SIMULATION = false;\nIS_OPTIMIZATION = false;\nUSE_JOYSTICK = true;\nJOYSTICK_MODE = 0; #   0 - normal, 1 - record, 2 - replay\nAUTO_RUN = false;\nAUTO_RUN_STATE = 0;    # -> WALKING, STANDING, POSING, SWIMMING\nENABLE_HIGH_LEVEL_CONTROLLER = false;\n"
            # config_str = data
            config_str = "WEBOTS CONTROL VARIABLES" + "\n"
            config_str = config_str + "requestedState=" + str(self.requestedState.data) + "\n"
            config_str = config_str + "# -> WALKING, STANDING, POSING, SWIMMING, ANIMAL_WALKING, ANIMAL_SWIMMING, ANIMAL_AQUASTEP, INITIAL" + "\n"
            config_str = config_str + "positionTarget= x:" + str(self.positionTarget.x)+" y:"+str(self.positionTarget.y)+" z:"+str(self.positionTarget.z)+ "\n"
            config_str = config_str + "gait=" + str(self.gait.data) + "\n"
            config_str = config_str + "controlCommand= x:" + str(self.controlCommand.x)+" y:"+ str(self.controlCommand.y)+" z:"+ str(self.controlCommand.z) + "\n"
            config_str = config_str + "x:translation direction, y:translation velocity, z:turning curvature" + "\n"+ "\n"
            config_str = config_str + "SETUP INFORMATION"+ "\n"
            config_str = config_str + "robotState=" + str(self.robotState.data) + "\n"
            config_str = config_str + "configSim=" + str(self.configSim.data) + "\n"
            config_str = config_str + "configHighLev=" + str(self.configHighLev.data) + "\n"
            config_str = config_str + "configJoy=" + str(self.configJoy.data) + "\n"
            config_str = config_str + "configMotors=" + str(self.configMotors.data) + "\n"
            self.config_pub.publish(config_str)
            rate.sleep()

    def bag_info_callback(self, msg):
        self.odom_trans.header.stamp = rospy.Time.now()
        self.odom_trans.transform.translation.x = 0
        self.odom_trans.transform.translation.y = 0
        self.odom_trans.transform.translation.z = 0
        q_orig = tf.transformations.quaternion_from_euler(self.RPY.z, self.RPY.y, self.RPY.x)
        self.odom_trans.transform.rotation = q_orig
        self.trans_broadcaster.sendTransform(
            (self.odom_trans.transform.translation.x, self.odom_trans.transform.translation.y,
             self.odom_trans.transform.translation.z), self.odom_trans.transform.rotation,
            rospy.Time.now(), self.odom_trans.child_frame_id, self.odom_trans.header.frame_id)
        self.joint_state.header.stamp = rospy.Time.now()

        pos_list = list(msg.position)
        pos_list[2] = -pos_list[2]
        pos_list[6] = -pos_list[6]
        pos_list[10] = -pos_list[10]
        pos_list[14] = -pos_list[14]
        pos_tuple = tuple(pos_list)

        self.joint_state.position = pos_tuple
        self.joint_state.effort = msg.effort
        self.joint_pub.publish(self.joint_state)

    def command_info_callback(self, msg):

        self.joint_errors.header.stamp = rospy.Time.now()
        self.joint_errors.position = tuple(
            numpy.subtract(msg.position, self.joint_state.position))
        self.joint_error_pub.publish(self.joint_errors)

    def control_command_callback(self, msg):
        self.controlCommand = msg

    def requested_state_callback(self, msg):
        self.requestedState = msg

    def gait_callback(self, msg):
        self.gait = msg

    def position_target_callback(self, msg):
        self.positionTarget = msg

    def robot_state_sub_callback(self, msg):
        self.robotState = msg
    
    def config_sim_callback(self, msg):
        self.configSim = msg
    
    def config_high_lev_callback(self, msg):
        self.configHighLev = msg
    
    def config_joy_callback(self, msg):
        self.configJoy = msg

    def config_motors_callback(self, msg):
        self.configMotors = msg

    def cmd_vel_callback(self, msg):
        point = Point()
        point.x=0 #direction
        point.y=msg.linear.x
        point.z=msg.angular.z
        self.control_command_pub.publish(point)

    def handle_button_state_0(self, req):
        self.requested_state_pub.publish(0)
        return buttonResponse()
    def handle_button_state_1(self, req):
        self.requested_state_pub.publish(1)
        return buttonResponse()
    def handle_button_state_2(self, req):
        self.requested_state_pub.publish(2)
        return buttonResponse()
    def handle_button_state_3(self, req):
        self.requested_state_pub.publish(3)
        return buttonResponse()
    def handle_button_state_4(self, req):
        self.requested_state_pub.publish(4)
        return buttonResponse()
    def handle_button_state_5(self, req):
        self.requested_state_pub.publish(5)    
        return buttonResponse()                       
    def handle_button_state_6(self, req):
        self.requested_state_pub.publish(6)
        return buttonResponse()
    def handle_button_state_7(self, req):
        self.requested_state_pub.publish(7)    
        return buttonResponse()  
    def rpy_callback(self, msg):
        self.RPY = msg
        q_orig = tf.transformations.quaternion_from_euler(self.RPY.z, self.RPY.y, self.RPY.x)
        self.odom_trans.transform.rotation = q_orig
        self.trans_broadcaster.sendTransform(
            (self.odom_trans.transform.translation.x, self.odom_trans.transform.translation.y,
             self.odom_trans.transform.translation.z), self.odom_trans.transform.rotation,
            rospy.Time.now(), self.odom_trans.child_frame_id,
            self.odom_trans.header.frame_id)  # was, "base_link", "odom"

    def pose_callback(self, msg):
        self.pose = msg.pose

    def find(self, name, path):
        for root, dirs, files in os.walk(path):
            if name in files:
                return os.path.join(root, name)


if __name__ == '__main__':
    try:
        rospy.init_node('bag_to_joint_state', anonymous=True)
        bag_to_joint_state = BagToJointState()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
