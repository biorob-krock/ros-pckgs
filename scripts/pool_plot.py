#!/usr/bin/env python

""" ROS node to broadcast and visualize point tracking in Biorob pool room

Currently configured for Krock 2 robot along with 4-LED fiducial (PCB by Alessandro). 
"""

import rospy
from std_msgs.msg import String, Int8
from geometry_msgs.msg import Point
from sensor_msgs.msg import PointCloud
import message_filters
from multiprocessing import Process

import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation
from matplotlib.patches import Rectangle

class PoolVis: 
    """ Container for stashing points and calculations from left/right cameras."""

    def __init__(self):
        self.x = [None]*20
        self.y = [None]*20
        self.centLx = 0.0
        self.centLy = 0.0
        self.centRx = 0.0
        self.centRy = 0.0
        self.nPnts = 0

    def cachePoints(self, pt_cloud):
        self.x = [None]*20
        self.y = [None]*20
        self.nPnts = len(pt_cloud.points)
        if self.nPnts > 0: 
            for i in range(self.nPnts):
                self.x[i] = pt_cloud.points[i].x
                self.y[i] = pt_cloud.points[i].y

    def centroid(self, pt_cloud):
        """ Caclulate the position (centroid) of the fiducial and pose

        Assumption here is that the further LED is where the "front" of the fidcial is pointing. 
        """
        self.centLx = 0.0
        self.centLy = 0.0
        self.centRx = 0.0
        self.centRy = 0.0
        PTS_FUDCUCIAL = 4

       
        # Calculate left/right cameras separately 
        cameras = ["left", "right"] 
        LR_IND = 100.0 # Point IDs from left camera start at 1.0, right camera starts at 101.0
        IND_CHANNEL = 0 # ROS topic Channel index that pt IDs are stored on

        for cam in cameras: 

            if cam == "left":
                ind_cam = [ind for ind, x in enumerate(pt_cloud.channels[IND_CHANNEL].values) if x < LR_IND]

            if cam == "right":
                ind_cam = [ind for ind, x in enumerate(pt_cloud.channels[IND_CHANNEL].values) if x > LR_IND]

            nPntsCam = len(ind_cam)
            if nPntsCam == PTS_FUDCUCIAL: # Don't calculate if we are tracking erroneous points
                pts_cam = np.empty([nPntsCam,2])

                for i in range(len(ind_cam)):
                    pts_cam[i,0]= pt_cloud.points[ ind_cam[i] ].x
                    pts_cam[i,1]= pt_cloud.points[ ind_cam[i] ].y
        

                centroid_cam = np.array([sum(pts_cam[:,0])/nPntsCam, sum(pts_cam[:,1])/nPntsCam ]) # sum up pts to get centroid
                npCentroid = np.ones([nPntsCam,1])*centroid_cam # 4x1 array w centroid values
                npDifference = pts_cam - np.ones([nPntsCam,1])*centroid_cam
                normPnts = np.linalg.norm(npDifference, axis=1)
                ind_dir = np.where( normPnts == max(normPnts) )     # "Direction" pt is the one furthest from centroid
                pts_direc = np.zeros([1,2])
                pts_direc[0,0] = pts_cam[ind_dir,0]
                pts_direc[0,1] = pts_cam[ind_dir,1]


                for pt in pts_cam:
                    if cam == "left":
                        print(pts_direc)
                        self.centLx = pts_direc[0,0]
                        self.centLy = pts_direc[0,1]

                    if cam == "right":
                        self.centRx = sum(pts_cam[:,0]) / nPntsCam
                        self.centRy = sum(pts_cam[:,1]) / nPntsCam 
                    

# First set up the figure, the axis, and the plot element we want to animate
fig = plt.figure()
ax = plt.axes(xlim=(0, 6), ylim=(0, 2))
left, bottom, width, height = (0, 0, 6, 2)
rect = plt.Rectangle((left, bottom), width, height,
                     facecolor="blue", alpha=0.2)
ax.axis('equal')
ax.add_patch(rect)
MARKER_SIZE = 2
line, = ax.plot([], [], 'go', ms=MARKER_SIZE)
centL, = ax.plot([1],[1],'ro',ms=MARKER_SIZE)
centR, = ax.plot([1],[1],'yo',ms=MARKER_SIZE)

poolData = PoolVis() 

def init():
    line.set_data([], [])
    centL.set_data([], [])
    centR.set_data([], [])
    return line, centL, centR,

# animation function.  This is called sequentially
def animate(i):
    line.set_data(poolData.x, poolData.y)
    centL.set_data(poolData.centLx, poolData.centLy)
    centR.set_data(poolData.centRx, poolData.centRy)
    # centR.set_data(poolData.centR[0], poolData.centR[1])
    return line, centL, centR,

def callback(pt_cloud):
    print("Number of points: ", len(pt_cloud.points))
    poolData.centroid(pt_cloud)
    poolData.cachePoints(pt_cloud)
        

def listener():
    rospy.init_node('pool_plot', anonymous=True)
    rospy.Subscriber("/pool_track/pt_cloud", PointCloud, callback)

    anim = animation.FuncAnimation(fig, animate, init_func=init,
                            frames=200, interval=100, blit=True)
    plt.show()

if __name__ == '__main__':
    listener()
    