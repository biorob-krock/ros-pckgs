/**
  * \file   pool_tracking.cpp
  * \author Matt Estrada
  * \date   March 2020
  * \brief  Example program to demonstrate the use of the tracking client class
  */

#include "ros/ros.h"
#include "std_msgs/Int8.h"
#include "geometry_msgs/Point.h"
#include "sensor_msgs/PointCloud.h"
#include "sensor_msgs/PointCloud2.h"

#include <sstream>
#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include "trkcli.h"
#include <sys/time.h>
#include <fstream>

using namespace std;


/** \brief Publish pool tracking points to ROS topic 
 * 
 * \todo Handle case where no points are found
 * 
 */
int main(int argc, char **argv)
{

  ros::init(argc, argv, "pool_tracking");
  
   // declares an instance of the tracking client
  CTrackingClient trk;

  time_t rawtime;
  struct tm * timeinfo;
  time (&rawtime);
  timeinfo = localtime (&rawtime);

  // connects to the server
  if (!trk.connect("biorobpc6.epfl.ch", 10502)) {
    return 1; 
  }
  ros::NodeHandle n;

  ros::Publisher pt_cnt_pub = n.advertise<std_msgs::Int8>("/pool_track/pt_cnt", 1000);
  ros::Publisher pt_cloud_pub = n.advertise<sensor_msgs::PointCloud>("/pool_track/pt_cloud", 1000);

  ros::Rate loop_rate(10);

  while (ros::ok())
  {

    uint32_t t;
    const track_point* pt_trk;
    int cnt_trk;

    if (!trk.update(t)) {
      cerr << "Failed to update points from the tracking server, quitting." << endl;
      return 3;
    }

    pt_trk = trk.get_pos_table(cnt_trk);

/*    if (pt_trk) {
      cout << cnt_trk << " points in table." << endl;
    }
*/
    std_msgs::Int8 pt_cnt_msg; // number of points returned 
    pt_cnt_msg.data = cnt_trk;

    sensor_msgs::PointCloud cloud; 

    cloud.header.stamp = ros::Time::now();
    cloud.header.frame_id = "pool_frame";

    cloud.channels.resize(1);
    cloud.channels[0].name = "id";
    cloud.channels[0].values.resize(cnt_trk);
    cloud.points.resize(cnt_trk);

    for (int i=0; i < cnt_trk; i++){
      cloud.points[i].x = pt_trk[i].x;
      cloud.points[i].y = pt_trk[i].y;
      cloud.points[i].z = 0;
      cloud.channels[0].values[i] = pt_trk[i].id;
    }  

    // ROS_INFO("%d", pt_cnt_msg.data);

    pt_cnt_pub.publish(pt_cnt_msg);
    pt_cloud_pub.publish(cloud);
    ros::spinOnce();

    loop_rate.sleep();

  }

  return 0;
}
