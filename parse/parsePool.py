#!/usr/bin/env python

import rosbag
from sensor_msgs.msg import PointCloud
from geometry_msgs.msg import Point
import rospy 

from matplotlib import pyplot as plt
from colorama import Fore, Back, Style # color printouts 
from numpy import linalg as LA
import numpy as np 

# Shelve
import shelve
import os.path

filename = '../data/bags/drag-experiment-2020-07-22_11-23-20.bag'

bag = rosbag.Bag(filename)

class poolTrack: 

    def __init__(self):
        self.secs = []
        self.nsecs = []
        self.nPnts = [] 
        self.points = []

poolBag = poolTrack()
shelf_filename='pool.shelve'
pickle_filename = 'poolBag.pickle'

starTime = rospy.Time(1584037820, 0) # 1584037824 is end of file

print(Fore.CYAN + 'Parsing file' +  Style.RESET_ALL)

for topic, msg, t in bag.read_messages(topics=['/pool_track/pt_cloud'], start_time=starTime):
    print(msg.header.stamp.secs)
    poolBag.nPnts.append( len(msg.points) )
    poolBag.secs.append( msg.header.stamp.secs)
    poolBag.nsecs.append( msg.header.stamp.nsecs )
    poolBag.points.append(msg.points)

bag.close()
print(Fore.CYAN + 'Parsed Bagfile' +  Style.RESET_ALL)

###################################
# Plot
###################################

fig = plt.figure()
plt.plot(poolBag.nPnts)

###################################
# Calc position 
###################################

n4 = []

for n in poolBag.nPnts:
    n4.append(n==4)
    # poolBag.nPnts.append( len(msg.points) )
    # poolBag.secs.append( msg.header.stamp.secs)
    # poolBag.nsecs.append( msg.header.stamp.nsecs )
    # poolBag.points.append(msg.points)

for p in poolBag.points:
    if len(p) == 4:
        l2 = [0]*4
        for i in range(len(p)): 
            for j in range(len(p)):
                l2[i] += LA.norm( [ p[j].x - p[i].x, p[j].y - p[i].y, p[j].z - p[i].z] ) 
        print(l2) 

print(n4)

plt.show()
