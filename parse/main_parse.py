#!/usr/bin/env python

from matplotlib import pyplot as plt
from parseRobot import KrockBag
import ipdb

# filename = '../data/bags/urdf-playback-2020-03-11_15-49-39.bag'
filename = '../data/bags/swim-ramp-first-2020-05-20_11-38-47.bag'

# krock2 = KrockBag(filename, partial_bag=5.0)
krock2 = KrockBag(filename)
# krock2.gen_plot('limb_effort','motor_current', 'energy','filter_test')
krock2.gen_plot('filter_test_sos', 'fft')

plt.ion()
plt.show()
ipdb.set_trace()
