Biorob laptop URI: `biorobB207457:37101`

water_experiments.launch 

## Logitech c920 and usb_cam

### Install usb_cam 

    sudo apt install ros-melodic-usb-cam

    sudo apt install ros-melodic-image-view

### Logitech Camera

[Logitech c920](https://www.logitech.com/en-ch/product/hd-pro-webcam-c920) webcam and viewing through 

    roslaunch usb_cam usb_cam-test.launch 

[image_view](http://wiki.ros.org/image_view#image_view.2BAC8-diamondback.video_recorder) has a useful video stream capture named video_recorder

    rosrun image_view video_recorder image:=/usb_cam/image_raw

To just **view** is simpler 

    rosrun image_view image_view image:=/usb_cam/image_raw


[Refocusing](https://www.youtube.com/watch?v=94lDYZgihT4) can be done manually by unscrewing housing and twisting lense. 