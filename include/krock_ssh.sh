    #!/bin/bash
     
    # connectkrock.sh 
    # Matt Estrada	January 27, 2020 	Lausanne, CH 
     
    # bash script to connect to Krock 2 robot over BioRob router
    # Useful how-to
    # 	https://medium.com/@LiliSousa/web-scraping-with-bash-690e4ee7f98d
     

    ##################################
    # Coloring
    ##################################
    # Black        0;30     Dark Gray     1;30
    # Red          0;31     Light Red     1;31
    # Green        0;32     Light Green   1;32
    # Brown/Orange 0;33     Yellow        1;33
    # Blue         0;34     Light Blue    1;34
    # Purple       0;35     Light Purple  1;35
    # Cyan         0;36     Light Cyan    1;36
    # Light Gray   0;37     White         1;37
    # example: $ printf "I ${RED}love${NC} Stack Overflow\n"

    RED='\033[0;31m'
    GREEN='\033[1;32m'
    REDL='\033[1;31m' # "light red"
    CYANL='\033[1;36m'
    PURP='\033[1;35m'
    NC='\033[0m'        # No Color
     
    ##################################
    # Scrape IP    
    ##################################
    BIOROBURL="https://biorob2.epfl.ch/utils/dynip.php" # URL for connected robots within BioRob  
    CYAN_TAG="Krockbot-XU4-Ubuntu18"
    MAGENTA_TAG="Krockbot-XU4-magenta"
    TEMP_FILE="tmp.txt"
    IP_IND="1" # Internal IP address happens to be this indice
    
    THIS_DIR= pwd | tr -d '\n' # strip off any new line characters
    SSHFS_DIR=$THIS_DIR$"/mnt"
     
    printf "\n\n${GREEN}Connecting to robot through krock.sh script ${NC} \n\n"

     # curl the page and save content to tmp.txt, -s for silent
    curl -s $BIOROBURL > $TEMP_FILE
     
    # Find Cyan 
    LINE=$(cat $TEMP_FILE | grep $CYAN_TAG) # Pick out line with Krock tag 
    IFS=',' # comma is set as delimiter
    read -ra ADDR <<< "$LINE" # $Line is read into an array as tokens separated by IFS
    CYAN_IP=${ADDR[$IP_IND]}
     
    printf "Cyan IP address is ${CYANL}$CYAN_IP${NC}\n"
     
    # Find Magenta 
    LINE=$(cat $TEMP_FILE | grep $MAGENTA_TAG) # Pick out line with Krock tag 
    IFS=',' # comma is set as delimiter
    read -ra ADDR <<< "$LINE" # $Line is read into an array as tokens separated by IFS
    MAGENTA_IP=${ADDR[$IP_IND]}
     
    printf "Magenta IP address is ${PURP} $MAGENTA_IP ${NC}\n\n"
     
    rm $TEMP_FILE # delete file 


     if [ $# -gt 0 ] # if we have arguments 
     then 

        ##################################
        # CONNECT   
        ##################################
         if [ $1 == "connect" ]
         then

            ##################################
            # SSHFS Mount & SSH into terminal    
            ##################################
            # NOTE: I assuming that we have ssh public authentication
            # 	If not, the password will block you, I haven't coded it to handle this
            # Useful link on running commands over ssh 
            # 	https://www.shellhacks.com/ssh-execute-remote-command-script-linux/
             
            ssh -o "StrictHostKeyChecking=no" biorob@$CYAN_IP
        

        ##################################
        # DISCONNECT   
        ##################################

        elif [ $1 == "disconnect" ]
        then
            echo Shutting down CYAN
            TIME2CONNECT=10
            timeout $TIME2CONNECT ssh biorob@$CYAN_IP "sudo -S shutdown now" # timeout in case it's frozen or busy 
             
            PASS="odroid"
            echo Shutting down MAGENTA
            timeout $TIME2CONNECT ssh -o "StrictHostKeyChecking=no" odroid@$MAGENTA_IP "echo $PASS | sudo -S shutdown now"

            timeout $TIME2CONNECT umount $SSHFS_DIR

        ##################################
        # SSHFS   
        ##################################
        elif [ $1 == "sshfs" ]
        then
            echo Mounting $SSHFS_DIR

            if[[ ! -d $SSHFS_DIR ]] && mkdir $SSHFS_DIR

            # Mount sshfs dir 
            # StrictHostKeyChecking prevents an extra "ocnfnect (y/n)?" prompt
            sshfs -o "StrictHostKeyChecking=no" biorob@$CYAN_IP: $SSHFS_DIR 

        ##################################
        # UNRECOGNIZED COMMAND   
        ##################################
        elif [ $1 == "cyanip" ]
        then

            echo $CYAN_IP

        else
            printf "${REDL}Input argument not recognized${NC}\n\n"
        fi

    fi
